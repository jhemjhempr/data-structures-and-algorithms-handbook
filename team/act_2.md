# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Billionaire HAHAHA

## Jemyll Ramoya

👋 Millionaire or Billionaire — 💌 jemyllramoya@student.laverdad.edu.ph — Apalit, Pampanga

![alt act2_ramoya_jemyll.jpg](images/act2_ramoya_jemyll.jpg)

### Bio

**Good to know:** Better to be unknown, that's good.

**Motto:** Konting bato, konting semento.... Monumento!

**Languages:** English tsaka Tagalog HAHAHA

**Personality Type:** [Architect (INTJ-A)](https://www.16personalities.com/profiles/fd1fc7a8bade9)

<!-- END -->
